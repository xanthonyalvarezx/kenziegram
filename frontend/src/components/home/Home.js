import React from "react";
import { useState } from "react";
import { uploadPhoto } from "../../../src/api";
import { Button } from "react-bootstrap";

const Home = () => {
  const [selectedFile, setSelectedFile] = useState(null);
  const [uploadStatus, setUploadStatus] = useState(null);

  const submitForm = async (event) => {
    try {
      event.preventDefault();
      await uploadPhoto(selectedFile);
      setUploadStatus("Success!");
    } catch {
      setUploadStatus("File too large: file must be 2mb or smaller!");
    } finally {
      setTimeout(() => {
        setUploadStatus(null);
      }, 3000);
    }
  };

  const onSelectFile = (event) => {
    setSelectedFile(event.target.files[0]);
  };
  return (
    <div>
      {uploadStatus && <p>{uploadStatus}</p>}
      <form onSubmit={submitForm}>
        <input type="file" name="photo" onChange={onSelectFile} />
        <Button type="submit" variant="primary">
          Submit
        </Button>
      </form>
    </div>
  );
};

export default Home;
