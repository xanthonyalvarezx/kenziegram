import React from "react";
import { fetchPhotos, fetchSinglePhoto } from "../../../src/api";

import { useEffect, useState } from "react";
import { Modal, Card } from "react-bootstrap";

const Photos = () => {
  const [photos, setPhotos] = useState([]);
  const [show, setShow] = useState(false);
  const [getData, setData] = useState({});

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  useEffect(() => {
    async function getPhotos() {
      setPhotos(await fetchPhotos());
    }
    getPhotos();
  }, []);

  const showPhoto = (event) => {
    handleShow();
    getModalPicture(event);
  };

  async function getModalPicture(event) {
    setData(await fetchSinglePhoto(event.target.src.substr(30)));
  }

  return (
    <div>
      <>
        <Modal show={show} onHide={handleClose} animation={false}>
          <Modal.Header closeButton></Modal.Header>
          <Modal.Title></Modal.Title>
          <hr />
          <Modal.Body>
            <Card style={{ width: "18rem" }}>
              <Card.Title>{getData.src}</Card.Title>
              <hr />
              <Card.Img
                variant="top"
                src={`http://localhost:5000/uploads/${getData.src} `}
                width="300px"
                height="200px"
              />
              <Card.Body></Card.Body>
            </Card>
          </Modal.Body>
          <hr />
          <Modal.Footer>{getData.size} kb</Modal.Footer>
        </Modal>

        <div id="imageList">
          {photos.map((pic) => (
            <img
              onClick={showPhoto}
              key={pic}
              src={`http://localhost:5000/${pic}`}
              alt={pic}
              width="250px"
              height="175px"
            />
          ))}
        </div>
      </>
    </div>
  );
};

export default Photos;
