import React from "react";
import { Link } from "react-router-dom";
import { Dropdown } from "react-bootstrap";

class Navigation extends React.Component {
  render() {
    return (
      <>
        <Dropdown id="nav">
          <Dropdown.Toggle as={Link}>Click to see options</Dropdown.Toggle>
          <Dropdown.Menu>
            <Dropdown.Item>
              <Link to="/">Home</Link>
            </Dropdown.Item>
            <br />
            <Dropdown.Item>
              <Link to="/photos">Photos</Link>
            </Dropdown.Item>
          </Dropdown.Menu>
        </Dropdown>
      </>
    );
  }
}

export default Navigation;
