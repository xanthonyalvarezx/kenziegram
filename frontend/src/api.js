import axios from "axios";

export const API_BASE_URL = "http://localhost:5000";

const axiosInstance = axios.create({
  baseUrl: API_BASE_URL,
});
export default axiosInstance;

export const fetchPhotos = async () => {
  const { data } = await axiosInstance.get("http://localhost:5000/photos", {
    params: {
      size: 2000000,
    },
  });
  return data;
};

export const fetchSinglePhoto = async (pic) => {
  const { data } = await axiosInstance.get(`http://localhost:5000/photos/${pic}`);
  console.log({ data });
  return data;
};

export const uploadPhoto = async (data) => {
  const formData = new FormData();
  formData.append("photo", data);
  await axiosInstance.post("http://localhost:5000/photos", formData);
};
