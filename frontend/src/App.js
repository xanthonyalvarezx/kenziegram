// for members and roles check read me


import "./App.css";
import React from "react";
import Navigation from "./components/navigation/Navigation";
import { Route, Switch } from "react-router-dom";
import Home from "./components/home/Home";
import Photos from "./components/photo/Photos";
import defaultPage from "./components/404/default";
import Logo from "../src/components/photo/kenziegram.jpg";

function App() {
  return (
    <div className="App">
      <img id="logo" src={Logo} alt="kenziegram logo" width="100px"></img>
      <Navigation />
      <div>
        <Switch>
          <Route exact path="/" component={Home} />
          <Route exact path="/photos" component={Photos} />
          <Route path="*" component={defaultPage} />
        </Switch>
      </div>
    </div>
  );
}

export default App;
