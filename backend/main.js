import express, { json } from "express";
import cors from "cors";
import path from "path";
import { uploader, UPLOAD_DIRECTORY, getUploadedFiles, findUploadedFile } from "./utils";

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));
app.use("/uploads", express.static(UPLOAD_DIRECTORY));

//get req 2 (fingers crossed so hard)

app.get("/photos/:filename", async (req, res, next) => {
  try {
    const { birthtime, size } = await findUploadedFile(req.params.filename);
    res.status(200).json({ birthtime, size, src: req.params.filename });
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});

app.get("/photos", async (req, res) => {
  try {
    const files = await getUploadedFiles();
    const fileData = files.map((file) => `uploads/${file}`);
    if (!req.query.size) {
      res.status(200).json(fileData);
      return;
    }
    const size = Number.parseInt(req.query.size, 10);

    const promises = files.map((file) => findUploadedFile(file));
    const results = await Promise.all(promises);
    const photosArr = [];
    results.forEach((data, i) => {
      if (data.size <= size) {
        photosArr.push(fileData[i]);
      }
    });
    res.status(200).json(photosArr);
  } catch (err) {
    res.status(500).json({ message: err.message });
  }
});
//post request build starts at 00:31:10 in vinces demo video from monday
app.post("/photos", uploader.single("photo"), (req, res, err) => {
  res.status(201).json({
    photo: req.file.path,
  });
});

app.get("*", (req, res) => {
  res.status(404).json({
    message: " 404 - You Should've Taken That Left Turn At Albuquerque!",
  });
});

app.listen(5000, () => {
  console.log("express server running on port 5000");
});
