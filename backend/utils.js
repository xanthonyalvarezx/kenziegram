import multer from "multer";
import path from "path";
import { v4 } from "uuid";
import fs from "fs/promises";

export const UPLOAD_DIRECTORY = "./uploads";

const checkIfDirectoryExists = async () => {
  try {
    await fs.stat(UPLOAD_DIRECTORY);
  } catch (err) {
    console.error(err);
    if (err.code === "ENOENT") {
      await fs.mkdir(UPLOAD_DIRECTORY);
    }
  }
};

const storage = multer.diskStorage({
  async destination(req, file, callback) {
    try {
      await checkIfDirectoryExists();
      callback(null, UPLOAD_DIRECTORY);
    } catch (err) {
      console.error(err);
      callback(err);
    }
  },

  async filename(req, file, callback) {
    const fileExtention = path.extname(file.originalname);
    const filename = `${v4()}${fileExtention}`;
    callback(null, filename);
  },
});

export const uploader = multer({
  storage,
  limits: { fileSize: 2000000 },
});

export const getUploadedFiles = async () => {
  return await fs.readdir(UPLOAD_DIRECTORY);
};

export const findUploadedFile = async (fileName) => {
  const info = await fs.stat(path.resolve(UPLOAD_DIRECTORY, fileName));
  return info;
};
